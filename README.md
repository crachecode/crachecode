# Pile [Jamstack](https://jamstatic.fr/2019/02/07/c-est-quoi-la-jamstack/) pour la gestion du site www.crachecode.net

basé sur [Jekyll](https://jekyllrb.com)

- front-end : [www.crachecode.net](https://www.crachecode.net)

## développement

```
git clone --recurse-submodules git@gitlab.com:crachecode/crachecode.git
cd crachecode
docker compose up
```

[http://crachecode.localhost](http://crachecode.localhost/)
