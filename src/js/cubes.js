import {
	Scene,
	PerspectiveCamera,
	WebGLRenderer,
	BoxGeometry,
	TextureLoader,
	MeshBasicMaterial,
	Mesh,
	Object3D
} from '../vendor/three.js/build/three.module.js'

export default function () {
	
	let container;
	let camera, scene, renderer;
	//let geometry;
	let groups = [];
	let mouseX = 0, mouseY = 0;
	let windowHalfX = window.innerWidth / 2;
	let windowHalfY = window.innerHeight / 2;
	let ind_speed_rot = 0.01;
	let global_wave = 0.2;
	let mouse_sens = 0.1;

	document.addEventListener('DOMContentLoaded', () => {
		document.addEventListener('mousemove', onDocumentMouseMove, false)
		init()
		animate()
	}, false)

	function init() {
		
		container = document.getElementById("background")
		camera = new PerspectiveCamera(60, window.innerWidth / window.innerHeight, 1, 10000)
		camera.position.z = 200
		scene = new Scene()
		renderer = new WebGLRenderer({antialias: true, alpha: true})

		let geometry = new BoxGeometry(100, 100, 100)
		let solid = new MeshBasicMaterial({color: 0xfeff00})
		let loader = new TextureLoader()
		let maxAnisotropy = renderer.capabilities.getMaxAnisotropy()

		function addCubes(texture) {
			texture.anisotropy = maxAnisotropy
			let material = new MeshBasicMaterial({
				map: texture
			});
			let materials = [material, material, solid, solid, material, material]
			for (let i = 0; i < 50; i++){
				let cube = new Mesh(geometry, materials)
				cube.position.x = Math.random() * 2000 - 1000
				cube.position.y = Math.random() * 2000 - 1000
				cube.position.z = Math.random() * 2000 - 1000
				cube.rotation.x = Math.random() * 2 * Math.PI
				cube.rotation.y = Math.random() * 2 * Math.PI
				cube.random1 = Math.random()
				cube.random2 = Math.random()
				let group = new Object3D()
				group.random1 = Math.random()
				group.random2 = Math.random()
				group.random3 = Math.random()
				group.add(cube)
				groups.push(group)
				scene.add(group)
			}
		}

		loader.load(
			'/img/icon_app.png',
			function (texture) {
				addCubes(texture)
			}
		);
		loader.load(
			'/img/icon_portfolio.png',
			function (texture) {
				addCubes(texture)
			}
		);
		loader.load(
			'/img/icon_vitrine.png',
			function (texture) {
				addCubes(texture)
			}
		);
		loader.load(
			'/img/icon_ecommerce.png',
			function (texture) {
				addCubes(texture)
			}
		)

		renderer.setPixelRatio(window.devicePixelRatio)
		renderer.setSize(window.innerWidth, window.innerHeight)
		container.appendChild(renderer.domElement)

		window.addEventListener('resize', onWindowResize, false)
	}


	function onWindowResize() {
		windowHalfX = window.innerWidth / 2
		windowHalfY = window.innerHeight / 2
		camera.aspect = window.innerWidth / window.innerHeight
		camera.updateProjectionMatrix()
		renderer.setSize(window.innerWidth, window.innerHeight)
	}

	function onDocumentMouseMove(event) {
		mouseX = (event.clientX - windowHalfX) * 10
		mouseY = (event.clientY - windowHalfY) * 10
	}

	function animate() {
		setTimeout( () => {
			requestAnimationFrame( animate )
		}, 1000 / 60 )
		render()
	}

	function render() {
		camera.position.x += ((mouseX * mouse_sens) - camera.position.x) * .05
		camera.position.y += ((-mouseY * mouse_sens) - camera.position.y) * .05
		camera.lookAt(scene.position)
		let currentSeconds = Date.now()
		for (let i = 0; i < groups.length; i++){
			let group = groups[i]
			group.rotation.x = Math.sin(currentSeconds * 0.0007 * group.random1) * global_wave
			group.rotation.y = Math.sin(currentSeconds * 0.0004 * group.random2) * global_wave
			group.rotation.z = Math.sin(currentSeconds * 0.0002 * group.random3) * global_wave
			for (let j = 0; j < group.children.length; j++){
				let cube = group.children[j]
				cube.rotation.x += Math.sin(cube.random1) * ind_speed_rot
				cube.rotation.y += Math.sin(cube.random2) * ind_speed_rot
			}
		}
		renderer.render(scene, camera)
	}
}
